package main

import (
	"context"
	"flag"
	"fmt"
	"io"
	"net/http"
	"os"
	"strconv"

	"go.jolheiser.com/xkcd"
)

var (
	imageFlag bool
	outFlag   string
)

func main() {
	flag.BoolVar(&imageFlag, "image", false, "get the image response for a comic")
	flag.StringVar(&outFlag, "out", "", "where to write the response (default: stdout)")
	flag.Parse()

	comicNum := 0
	if flag.NArg() > 0 {
		arg := flag.Arg(0)
		i, err := strconv.Atoi(arg)
		if err != nil {
			fmt.Printf("Specific comic must be a number: %v\n", err)
			return
		}
		comicNum = i
	}
	out := os.Stdout
	if outFlag != "" {
		fi, err := os.Create(outFlag)
		if err != nil {
			fmt.Printf("Could not create file %s: %v\n", outFlag, err)
			return
		}
		defer fi.Close()
		out = fi
	}

	if err := run(comicNum, out); err != nil {
		fmt.Println(err)
	}
}

func run(comicNum int, out io.WriteCloser) error {

	client := xkcd.New()
	comic, err := client.Comic(context.Background(), comicNum)
	if err != nil {
		return err
	}

	if imageFlag {
		return image(comic.Img, out)
	}

	format := `Comic: %d
Date: %s/%s/%s
Title: %s
Alt: %s
Image: %s
`
	if _, err := fmt.Fprintf(out, format,
		comic.Num,
		comic.Month, comic.Day, comic.Year,
		comic.Title,
		comic.Alt,
		comic.Img); err != nil {
		return err
	}
	return nil
}

func image(img string, out io.WriteCloser) error {
	resp, err := http.Get(img)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	if _, err := io.Copy(out, resp.Body); err != nil {
		return err
	}
	return nil
}
