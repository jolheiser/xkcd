package xkcd

import (
	"context"
	"math"
	"net/http"
	"net/http/httptest"
	"os"
	"strings"
	"testing"
)

var (
	server *httptest.Server
	client *Client

	comicCurrentResp = []byte(`{"month": "1", "num": 2407, "link": "", "year": "2021", "news": "", "safe_title": "Depth and Breadth", "transcript": "", "alt": "A death-first search is when you lose your keys and travel to the depths of hell to find them, and then if they're not there you start checking your coat pockets.", "img": "https://imgs.xkcd.com/comics/depth_and_breadth.png", "title": "Depth and Breadth", "day": "4"}`)
	comic1597Resp    = []byte(`{"month": "10", "num": 1597, "link": "", "year": "2015", "news": "", "safe_title": "Git", "transcript": "[[3 people stand in front of a laptop on a desk.  Person 3 just stands there and learns]]\nPerson 1: This is git. It tracks collaborative work on projects through a beautiful distributed graph theory tree model.\nPerson 2: Cool. How do we use it?\nPerson 1: No idea. Just memorize these shell commands and type them to sync up.\nIf you get errors, save your work elsewhere, delete the project, and download a fresh copy.\n\n{{Title text: If that doesn't fix it, git.txt contains the phone number of a friend of mine who understands git. Just wait through a few minutes of 'It's really pretty simple, just think of branches as...' and eventually you'll learn the commands that will fix everything.}}", "alt": "If that doesn't fix it, git.txt contains the phone number of a friend of mine who understands git. Just wait through a few minutes of 'It's really pretty simple, just think of branches as...' and eventually you'll learn the commands that will fix everything.", "img": "https://imgs.xkcd.com/comics/git.png", "title": "Git", "day": "30"}`)

	comicCurrent = Comic{
		Month:      "1",
		Num:        2407,
		Link:       "",
		Year:       "2021",
		News:       "",
		SafeTitle:  "Depth and Breadth",
		Transcript: "",
		Alt:        "A death-first search is when you lose your keys and travel to the depths of hell to find them, and then if they're not there you start checking your coat pockets.",
		Img:        "https://imgs.xkcd.com/comics/depth_and_breadth.png",
		Title:      "Depth and Breadth",
		Day:        "4",
	}
	comic1597 = Comic{
		Month:      "10",
		Num:        1597,
		Link:       "",
		Year:       "2015",
		News:       "",
		SafeTitle:  "Git",
		Transcript: "[[3 people stand in front of a laptop on a desk.  Person 3 just stands there and learns]]\nPerson 1: This is git. It tracks collaborative work on projects through a beautiful distributed graph theory tree model.\nPerson 2: Cool. How do we use it?\nPerson 1: No idea. Just memorize these shell commands and type them to sync up.\nIf you get errors, save your work elsewhere, delete the project, and download a fresh copy.\n\n{{Title text: If that doesn't fix it, git.txt contains the phone number of a friend of mine who understands git. Just wait through a few minutes of 'It's really pretty simple, just think of branches as...' and eventually you'll learn the commands that will fix everything.}}",
		Alt:        "If that doesn't fix it, git.txt contains the phone number of a friend of mine who understands git. Just wait through a few minutes of 'It's really pretty simple, just think of branches as...' and eventually you'll learn the commands that will fix everything.",
		Img:        "https://imgs.xkcd.com/comics/git.png",
		Title:      "Git",
		Day:        "30",
	}
)

func TestMain(m *testing.M) {

	handler := func(w http.ResponseWriter, r *http.Request) {
		if r.URL.Path == "/info.0.json" {
			_, _ = w.Write(comicCurrentResp)
			return
		}
		if r.URL.Path == "/1597/info.0.json" {
			_, _ = w.Write(comic1597Resp)
			return
		}
		w.WriteHeader(http.StatusNotFound)
	}

	server = httptest.NewServer(http.HandlerFunc(handler))
	baseURL = server.URL + "/"
	client = New()

	os.Exit(m.Run())
}

func TestComic(t *testing.T) {
	t.Parallel()

	comic, err := client.Comic(context.Background(), 1597)
	if err != nil {
		t.Logf("could not get comic 1597: %v\n", err)
		t.FailNow()
	}

	if *comic != comic1597 {
		t.Log("comic does not match test data")
		t.FailNow()
	}
}

func TestCurrent(t *testing.T) {
	t.Parallel()

	comic, err := client.Current(context.Background())
	if err != nil {
		t.Logf("could not get current comic: %v\n", err)
		t.FailNow()
	}

	if *comic != comicCurrent {
		t.Log("comic does not match test data")
		t.FailNow()
	}
}

func TestInvalid(t *testing.T) {
	t.Parallel()

	_, err := client.Comic(context.Background(), math.MaxInt16)
	if err == nil {
		t.Log("nothing should be returned for an invalid comic")
		t.FailNow()
	}

	if !strings.EqualFold(err.Error(), "got non-200 response: 404 Not Found") {
		t.Log("did not get a 404")
		t.FailNow()
	}
}
