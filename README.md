# XKCD

An XKCD API and CLI.

You can find the API information on the [about page](https://xkcd.com/about/) near the bottom.

## Library

```go
package main

import (
	"context"
	
	"gitea.com/jolheiser/xkcd"
)

func main() {
	client := xkcd.New()
	comic, err := client.Current(context.Background())
	if err != nil {
		panic(err)
	}
	
	// Do stuff...
}
```

## CLI

[Build](Makefile) the [CLI](cmd/xkcd/main.go) if you think it's useful.

If no comic number is given, it grabs the current comic.

`./xkcd [--image] [--out=<path>] [<comic>]`

* `--image` - Get the image from the returned image URL. Pipe it wherever you want, or...
* `--out <path>` - A path to a file to store the response (or image)

## License

[MIT](LICENSE)