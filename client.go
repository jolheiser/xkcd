package xkcd

import "net/http"

// Client is an XKCD client
type Client struct {
	http *http.Client
}

// New returns a new XKCD Client
func New(opts ...ClientOption) *Client {
	c := &Client{
		http: http.DefaultClient,
	}
	for _, opt := range opts {
		opt(c)
	}
	return c
}

// ClientOption is options for a Client
type ClientOption func(*Client)

// WithHTTP sets the http.Client for the XKCD Client
func WithHTTP(client *http.Client) ClientOption {
	return func(c *Client) {
		c.http = client
	}
}
